package com.sanmisha.mynews;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Select Button we want to change the Font
        Button button = (Button) findViewById(R.id.bar);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/FontAwesome.ttf");
        //Set the typeface
        button.setTypeface(font);
    }
}
